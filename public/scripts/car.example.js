class Car {
  static list = [];
          
  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({ id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div class="container">
      <div class="card p-3 mb-3 shadow-lg" style="height:600px">
        <img src=${this.image} alt="" style="height:250px; object-fit: cover;" class="shadow-sm">
        <div>
          <p class="fw-bold mt-1">${this.manufacture}/${this.type}</p>
        </div>
        <div>
          <p class="fw-bold">Rp. ${this.rentPerDay} / hari</p>
        </div>
        <div>
          <p class="description" title="${this.description}" style="
          overflow: hidden;
          white-space: nowrap;
          text-overflow: ellipsis;">${this.description}</p>
        </div>
        <div>
          <span><img src="../image/fi_users.png" alt="" style="height : 30px; weight:30px" class="pb-3 pr-2">${this.capacity} Orang</span>
        </div>
        <div>
          <span><img src="../image/fi_gear.png" alt="" style="height : 30px; weight:30px" class="pb-3 pr-2">${this.transmission}</span>
        </div>
        <div>
          <span><img src="../image/fi_calendar.png" alt="" style="height : 30px; weight:30px" class="pb-3 pr-2">Tahun ${this.year}</span>
        </div>
        <button class="btn btn-success"> Pilih Mobil</button>
      </div>
    </div>
  `;
  }
}
