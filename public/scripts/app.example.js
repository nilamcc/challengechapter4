class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    this.clear();
    const passenger = document.getElementById("passenger");
    const date = document.getElementById("date");
    const time = document.getElementById("time");

    const passengerSeat = passenger.value;
    const dateRent = date.value;
    const timeRent = time.value;
    
    let inputDateTime = Date.parse(dateRent + "T" + timeRent + "Z");
    
    const cars = await Binar.listCars();
    
    const availableCar = cars.filter((car) => {
      return car.capacity >= passengerSeat && car.available === true && parseInt(Date.parse(car.availableAt)) > parseInt(inputDateTime);
    });
    Car.init(availableCar);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}